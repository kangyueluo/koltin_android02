package com.example.kotlin_android02

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var btn_costom_toast : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Toast.makeText(application,"Toast Hello",Toast.LENGTH_SHORT).show()

        btn_costom_toast = findViewById(R.id.btn_custom_toast) as Button

        btn_costom_toast.setOnClickListener(View.OnClickListener {
            if(it.id == R.id.btn_custom_toast){
                val inflater = layoutInflater
                val layout = inflater.inflate(R.layout.custom_toast,findViewById(R.id.custom_toast))

                val toast : Toast = Toast(applicationContext)
                toast.setGravity(Gravity.CENTER,0,0)
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout)
                toast.show()

            }
        })
    }
}
